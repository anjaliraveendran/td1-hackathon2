import http.client
import requests

SLACK_URL = 'https://app.slack.com/client/T019E9M05RU/D01A27J0A3T'
request = requests.get('http://www.example.com')

def post_message_to_slack(text):
    print('sending to slack:')
    print(text)

def main_simple():
    if request.status_code == 200:
        print('Web site exists')
    elif request.status_code == 404:
        print('Theres an error Ill send it the slack channel here')
    else:
        print('Youve got a different status code that this if statement doesnt even want to look at') 

if __name__ == '__main__':
    main_simple()