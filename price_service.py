import datetime
import logging

from flask import Flask
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr


LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })

@api.route('/v1/price/<ticker>')
@api.param('ticker', 'The stock ticker to get a price for')
class Price(Resource):
    def get(self, ticker):
        start_date = datetime.datetime.now() - datetime.timedelta(1)

        try:
            df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
        except Exception as ex:
            LOG.error('Error getting data: ' + str(ex))
        
        LOG.error('Recieved price data:')
        LOG.error(str(df))

        df['Date'] = df.index

        return {'ticker': ticker,
                'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                'price': df.iloc[-1]['Close']}


@api.route('/v1/stock')
class Stock(Resource):

    @api.expect(stock)
    def post(self):
        LOG.error('Received payload:')
        LOG.error(api.payload)

        # store this record in a database

        return {'result': 'success',
                'ticker': api.payload['ticker']}
    
@api.route('advice/AAPL', methods= ['GET'])
class Bands(Resource):

   def get_request(price):
    return {'BUY', 'SELL', 'HOLD'}


if __name__ == '__main__':
    app.run(debug=True)
